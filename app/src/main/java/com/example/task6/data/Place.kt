package com.example.task6.data

import com.google.android.gms.maps.model.LatLng
import com.google.maps.android.clustering.ClusterItem

data class Place(
    val name: String,
    val address: String,
    val latLng: LatLng,
    val currency: String,
    val workTime: String
) : ClusterItem {
    override fun getPosition(): LatLng = latLng

    override fun getTitle(): String = name

    override fun getSnippet(): String = address
}