package com.example.task6.presentation.map.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.example.task6.R
import com.example.task6.common.GOMEL
import com.example.task6.data.Place
import com.example.task6.databinding.ActivityMapsBinding
import com.example.task6.presentation.map.model.MainViewModel
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.Circle
import com.google.android.gms.maps.model.CircleOptions
import com.google.android.gms.maps.model.LatLngBounds
import com.google.maps.android.clustering.ClusterManager
import org.koin.androidx.viewmodel.ext.android.viewModel

class MapsActivity : AppCompatActivity(), OnMapReadyCallback {

    private lateinit var binding: ActivityMapsBinding
    private val viewModel by viewModel<MainViewModel>()
    private var circle: Circle? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMapsBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val mapFragment = supportFragmentManager
            .findFragmentById(binding.map.id) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    override fun onMapReady(googleMap: GoogleMap) {
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(GOMEL, 10F))

        viewModel.placesList.observe(this@MapsActivity) { places ->
            if (places.isNotEmpty()) {
                addMarkers(googleMap, places)
                val bounds = LatLngBounds.builder()
                places.forEach { bounds.include(it.latLng) }
                googleMap.moveCamera(
                    CameraUpdateFactory.newLatLngBounds(
                        bounds.build(),
                        30
                    )
                )
            }
        }
    }

    private fun addMarkers(googleMap: GoogleMap, places: List<Place>) {
        ClusterManager<Place>(this@MapsActivity, googleMap).apply {
            renderer = PlaceRenderer(
                this@MapsActivity,
                googleMap,
                this
            )

            markerCollection.setInfoWindowAdapter(MarkerInfoWindowAdapter(this@MapsActivity))

            addItems(places)
            cluster()

            setOnClusterItemClickListener { item ->
                addCircle(googleMap, item)
                return@setOnClusterItemClickListener false
            }

            googleMap.setOnCameraMoveStartedListener {
                markerCollection.markers.forEach { it.alpha = 0.3f }
                clusterMarkerCollection.markers.forEach { it.alpha = 0.3f }
            }

            googleMap.setOnCameraIdleListener {
                markerCollection.markers.forEach { it.alpha = 1.0f }
                clusterMarkerCollection.markers.forEach { it.alpha = 1.0f }

                onCameraIdle()
            }
        }
    }

    private fun addCircle(googleMap: GoogleMap, item: Place) {
        circle?.remove()
        circle = googleMap.addCircle(
            CircleOptions()
                .center(item.latLng)
                .radius(1000.0)
                .strokeColor(ContextCompat.getColor(this, R.color.purple_700))
        )
    }
}