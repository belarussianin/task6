package com.example.task6.domain.use_case

import com.example.task6.domain.repository.BankRepository

class GetFilialsListByCityUseCase(
    private val repository: BankRepository
) {
    fun run(city: String) = repository.getFilialsByCity(city)
}