package com.example.task6.data.remote

import com.example.task6.data.remote.dto.AtmDto
import com.example.task6.data.remote.dto.FilialDto
import com.example.task6.data.remote.dto.InfoboxDto
import io.reactivex.rxjava3.core.Observable
import retrofit2.http.GET
import retrofit2.http.Query

interface BankAPI {
    @GET("atm?")
    fun getCityAtms(@Query("city") city: String): Observable<List<AtmDto>>

    @GET("infobox?")
    fun getCityInfoboxList(@Query("city") city: String): Observable<List<InfoboxDto>>

    @GET("filials_info?")
    fun getCityFilialList(@Query("city") city: String): Observable<List<FilialDto>>
}