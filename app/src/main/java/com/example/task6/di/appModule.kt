package com.example.task6.di

import com.example.task6.data.repository.BankRepositoryImpl
import org.koin.dsl.module

val appModule = module {
    //Repository
    single { BankRepositoryImpl(get()) }
}