package com.example.task6.domain.use_case

import com.example.task6.domain.repository.BankRepository

class GetAtmsListByCityUseCase(
    private val repository: BankRepository
) {
    fun run(city: String) = repository.getAtmsByCity(city)
}