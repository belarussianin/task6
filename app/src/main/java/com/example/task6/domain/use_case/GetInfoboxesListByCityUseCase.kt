package com.example.task6.domain.use_case

import com.example.task6.domain.repository.BankRepository

class GetInfoboxesListByCityUseCase(
    private val repository: BankRepository
) {
    fun run(city: String) = repository.getInfoboxesByCity(city)
}