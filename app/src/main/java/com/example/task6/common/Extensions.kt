package com.example.task6.common

import com.example.task6.data.Place
import com.example.task6.data.remote.dto.AtmDto
import com.example.task6.data.remote.dto.Dto
import com.example.task6.data.remote.dto.FilialDto
import com.example.task6.data.remote.dto.InfoboxDto
import com.google.android.gms.maps.model.LatLng
import kotlin.math.pow
import kotlin.math.sqrt

fun Dto.toPlace(): Place {
    return when (this) {
        is AtmDto -> this.toPlace()
        is FilialDto -> this.toPlace()
        is InfoboxDto -> this.toPlace()
    }
}

fun AtmDto.toPlace() = Place(
    name = install_place,
    address = "$address_type $address $house",
    latLng = LatLng(gps_x.toDouble(), gps_y.toDouble()),
    currency = currency,
    workTime = work_time
)

fun FilialDto.toPlace() = Place(
    name = filial_name ?: "",
    address = "$street_type $street $home_number",
    latLng = LatLng(GPS_X.toDouble(), GPS_Y.toDouble()),
    currency = bel_number_schet ?: "",
    workTime = info_worktime ?: ""
)

fun InfoboxDto.toPlace() = Place(
    name = install_place,
    address = "$address_type $address $house",
    latLng = LatLng(gps_x.toDouble(), gps_y.toDouble()),
    currency = currency,
    workTime = work_time
)

infix fun LatLng.calculateDistance(latLng: LatLng): Double {
    return sqrt((latitude - latLng.latitude).pow(2) + (longitude - latLng.longitude).pow(2))
}

fun List<Double>.minValues(count: Int): List<Double> {
    return when {
        count > size -> this
        count < 1 -> emptyList()
        else -> {
            val result = MutableList(count) { 0.0 }
            var currMin = Double.MAX_VALUE
            for (i in 0 until count) {
                for (j in indices) {
                    if (this[j] < currMin) {
                        if (i > 0) {
                            if (this[j] > result[i - 1]) {
                                currMin = this[j]
                            }
                        } else {
                            currMin = this[j]
                        }
                    }
                }
                result[i] = currMin
                currMin = Double.MAX_VALUE
            }
            result
        }
    }
}