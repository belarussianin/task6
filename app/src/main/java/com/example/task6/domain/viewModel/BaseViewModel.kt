package com.example.task6.domain.viewModel

import android.util.Log
import androidx.lifecycle.ViewModel
import com.example.task6.common.TAG
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.schedulers.Schedulers
import java.util.concurrent.TimeUnit

abstract class BaseViewModel : ViewModel() {
    fun <T : Any> handleNetworkObservable(
        observable: Observable<T>,
        retryDelay: Long,
        timeUnit: TimeUnit
    ): Observable<out T> {
        return observable
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .retryWhen { cause ->
                Log.e(TAG, "handleNetworkObservable: error")
                cause.delay(retryDelay, timeUnit, Schedulers.io())
            }
    }
}