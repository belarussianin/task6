package com.example.task6.presentation.map.model

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.task6.common.GOMEL
import com.example.task6.common.TAG
import com.example.task6.common.calculateDistance
import com.example.task6.common.minValues
import com.example.task6.common.toPlace
import com.example.task6.data.Place
import com.example.task6.domain.use_case.GetAtmsListByCityUseCase
import com.example.task6.domain.use_case.GetFilialsListByCityUseCase
import com.example.task6.domain.use_case.GetInfoboxesListByCityUseCase
import com.example.task6.domain.viewModel.BaseViewModel
import com.google.android.gms.maps.model.LatLng
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Observable.combineLatest
import io.reactivex.rxjava3.schedulers.Schedulers
import java.util.concurrent.TimeUnit

class MainViewModel(
    private val getAtmsListByCityUseCase: GetAtmsListByCityUseCase,
    private val getFilialsListByCityUseCase: GetFilialsListByCityUseCase,
    private val getInfoboxesListByCityUseCase: GetInfoboxesListByCityUseCase
) : BaseViewModel() {

    private val elementsCount = 10

    private val _placesList = MutableLiveData<List<Place>>(emptyList())
    val placesList: LiveData<List<Place>> get() = _placesList

    init {
        fetchRemoteDataByCity("Гомель")
    }

    fun fetchRemoteDataByCity(city: String) {
        combineLatest(
            handleNetworkObservable(getAtmsListByCityUseCase.run(city), 5, TimeUnit.SECONDS),
            handleNetworkObservable(getFilialsListByCityUseCase.run(city), 5, TimeUnit.SECONDS),
            handleNetworkObservable(getInfoboxesListByCityUseCase.run(city), 5, TimeUnit.SECONDS)
        ) { atmsList, filiasList, infoboxesList ->
            Schedulers.io().scheduleDirect {
                val resultList =
                    atmsList.map {
                        GOMEL calculateDistance LatLng(
                            it.gps_x.toDouble(),
                            it.gps_y.toDouble()
                        ) to it
                    }.plus(
                        filiasList.map {
                            GOMEL calculateDistance LatLng(
                                it.GPS_X.toDouble(),
                                it.GPS_Y.toDouble()
                            ) to it
                        }
                    ).plus(
                        infoboxesList.map {
                            GOMEL calculateDistance LatLng(
                                it.gps_x.toDouble(),
                                it.gps_y.toDouble()
                            ) to it
                        }
                    )

                val minDistances = resultList.map { it.first }.minValues(elementsCount)

                val result = HashMap<Place, Double>().apply {
                    putAll(
                        resultList.filter {
                            it.first in minDistances
                        }.map {
                            it.second.toPlace() to it.first
                        }
                    )
                }.toList().sortedBy { it.second }.map { it.first }.take(elementsCount)

                AndroidSchedulers.mainThread().scheduleDirect {
                    _placesList.value = result
                }
            }
        }.subscribe {
            Log.e(TAG, "fetchRemoteDataByCity: $city")
        }
    }
}