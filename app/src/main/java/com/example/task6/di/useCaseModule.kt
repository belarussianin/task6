package com.example.task6.di

import com.example.task6.data.repository.BankRepositoryImpl
import com.example.task6.domain.use_case.GetAtmsListByCityUseCase
import com.example.task6.domain.use_case.GetFilialsListByCityUseCase
import com.example.task6.domain.use_case.GetInfoboxesListByCityUseCase
import org.koin.dsl.module

val useCaseModule = module {
    factory { GetAtmsListByCityUseCase(get<BankRepositoryImpl>()) }
    factory { GetFilialsListByCityUseCase(get<BankRepositoryImpl>()) }
    factory { GetInfoboxesListByCityUseCase(get<BankRepositoryImpl>()) }
}