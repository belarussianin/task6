package com.example.task6.presentation.map.ui

import android.content.Context
import com.example.task6.R
import com.example.task6.data.Place
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.BitmapDescriptor
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.google.maps.android.clustering.ClusterManager
import com.google.maps.android.clustering.view.DefaultClusterRenderer

class PlaceRenderer(
    private val context: Context,
    map: GoogleMap,
    clusterManager: ClusterManager<Place>
) : DefaultClusterRenderer<Place>(context, map, clusterManager) {

    private val atmIcon: BitmapDescriptor by lazy {
        com.example.task6.common.BitmapHelper.vectorToBitmap(
            context,
            R.drawable.ic_baseline_monetization_on_24
        )
    }

    override fun onBeforeClusterItemRendered(
        item: Place,
        markerOptions: MarkerOptions
    ) {
        item.apply {
            markerOptions.title("${name}\n${address}\n${workTime}")
                .position(latLng)
                .icon(atmIcon)
        }
    }

    override fun onClusterItemRendered(clusterItem: Place, marker: Marker) {
        marker.tag = clusterItem
    }
}