package com.example.task6.data.repository

import com.example.task6.data.remote.BankAPI
import com.example.task6.data.remote.dto.AtmDto
import com.example.task6.data.remote.dto.FilialDto
import com.example.task6.data.remote.dto.InfoboxDto
import com.example.task6.domain.repository.BankRepository
import io.reactivex.rxjava3.core.Observable

class BankRepositoryImpl(
    private val bankAPI: BankAPI
) : BankRepository {
    override fun getAtmsByCity(city: String): Observable<List<AtmDto>> {
        return bankAPI.getCityAtms(city)
    }

    override fun getFilialsByCity(city: String): Observable<List<FilialDto>> {
        return bankAPI.getCityFilialList(city)
    }

    override fun getInfoboxesByCity(city: String): Observable<List<InfoboxDto>> {
        return bankAPI.getCityInfoboxList(city)
    }
}