package com.example.task6.presentation.map.ui

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import com.example.task6.data.Place
import com.example.task6.databinding.MarkerInfoContentsBinding
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.Marker

class MarkerInfoWindowAdapter(
    private val context: Context
) : GoogleMap.InfoWindowAdapter {

    override fun getInfoContents(marker: Marker): View? {
        val place = marker.tag as? Place ?: return null

        return MarkerInfoContentsBinding.inflate(LayoutInflater.from(context)).apply {
            markerTitle.text = place.name
            markerAddress.text = place.address
            markerCurrency.text = place.currency
            markerWorkTime.text = place.workTime
        }.root
    }

    override fun getInfoWindow(marker: Marker): View? {
        return null
    }
}