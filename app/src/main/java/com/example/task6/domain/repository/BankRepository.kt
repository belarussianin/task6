package com.example.task6.domain.repository

import com.example.task6.data.remote.dto.AtmDto
import com.example.task6.data.remote.dto.FilialDto
import com.example.task6.data.remote.dto.InfoboxDto
import io.reactivex.rxjava3.core.Observable

interface BankRepository {
    fun getAtmsByCity(city: String): Observable<List<AtmDto>>
    fun getFilialsByCity(city: String): Observable<List<FilialDto>>
    fun getInfoboxesByCity(city: String): Observable<List<InfoboxDto>>
}